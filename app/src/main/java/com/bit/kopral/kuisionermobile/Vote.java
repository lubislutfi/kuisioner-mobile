package com.bit.kopral.kuisionermobile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import CONST.CONFIG;
import CONST.CONSTANTS;
import util.AudioPlayerHelper;

public class Vote extends AppCompatActivity {

    RelativeLayout relLay_activity_vote;
    TextView tv_vote_pageindicator;
    TextView tv_vote_question;

    AudioPlayerHelper aph;

    int pagePos;

    JSONArray jsonArray_vote;
    JSONObject jsonObject_vote;
    JSONObject jsonObject_voteItem;

    //kebutuhan http request
    RequestQueue requestQueue;
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.vote);
        getSupportActionBar().hide();

        initialize();
    }

    public void voteClick(View v) {
        jsonObject_voteItem = new JSONObject();

        switch (v.getId()) {
            case R.id.linLay_vote_3:
                try {

                    jsonObject_voteItem.put("question_id", CONSTANTS.QUESTION_DOWNLOADED.getJSONObject(pagePos).getString("question_id"));
                    jsonObject_voteItem.put("value", "3");
                    jsonArray_vote.put(jsonObject_voteItem);
                    aph.playSoundFX("fx_click.wav");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (pagePos < CONSTANTS.QUESTION_DOWNLOADED.length() - 1) {
                    pagePos++;
                    showQuestion();
                } else {
                    try {
                        jsonObject_vote.put("user_id", CONSTANTS.ID);
                        jsonObject_vote.put("vote", (Object) jsonArray_vote);
                        //Log.d("JSON VOTE", jsonObject_vote.toString());
                        sendVote();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                break;
            case R.id.linLay_vote_2:
                try {

                    jsonObject_voteItem.put("question_id", CONSTANTS.QUESTION_DOWNLOADED.getJSONObject(pagePos).getString("question_id"));
                    jsonObject_voteItem.put("value", "2");
                    jsonArray_vote.put(jsonObject_voteItem);
                    aph.playSoundFX("fx_click.wav");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (pagePos < CONSTANTS.QUESTION_DOWNLOADED.length() - 1) {
                    pagePos++;
                    showQuestion();
                } else {
                    try {
                        jsonObject_vote.put("user_id", CONSTANTS.ID);
                        jsonObject_vote.put("vote", (Object) jsonArray_vote);
                        //Log.d("JSON VOTE", jsonObject_vote.toString());
                        sendVote();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.linLay_vote_1:
                try {

                    jsonObject_voteItem.put("question_id", CONSTANTS.QUESTION_DOWNLOADED.getJSONObject(pagePos).getString("question_id"));
                    jsonObject_voteItem.put("value", "1");
                    jsonArray_vote.put(jsonObject_voteItem);
                    aph.playSoundFX("fx_click.wav");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (pagePos < CONSTANTS.QUESTION_DOWNLOADED.length() - 1) {
                    pagePos++;
                    showQuestion();
                } else {
                    try {
                        jsonObject_vote.put("user_id", CONSTANTS.ID);
                        jsonObject_vote.put("vote", (Object) jsonArray_vote);
                        //Log.d("JSON VOTE", jsonObject_vote.toString());
                        sendVote();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    private void initialize() {
        aph = new AudioPlayerHelper(getBaseContext());
        jsonObject_vote = new JSONObject();
        jsonArray_vote = new JSONArray();
        requestQueue = Volley.newRequestQueue(this); // 'this' is the Context
        pDialog = new ProgressDialog(this);

        pagePos = 0;

        relLay_activity_vote = (RelativeLayout) findViewById(R.id.relLay_activity_vote);
        tv_vote_pageindicator = (TextView) findViewById(R.id.tv_vote_pageindicator);
        tv_vote_question = (TextView) findViewById(R.id.tv_vote_question);

        relLay_activity_vote.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                if (getSupportActionBar().isShowing()) {
                    getSupportActionBar().hide();
                } else {
                    getSupportActionBar().show();
                }
                return false;
            }
        });

        getQuestionData();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_vote, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent itn;
        switch (item.getItemId()) {
            case R.id.action_settings:
                itn = new Intent(Vote.this, Settings.class);
                itn.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(itn);
                return true;
            case R.id.action_backtomenu:
                itn = new Intent(Vote.this, Main.class);
                itn.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(itn);
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private void showQuestion() {
        try {
            tv_vote_pageindicator.setText((pagePos + 1) + " dari " + CONSTANTS.QUESTION_DOWNLOADED.length());
            tv_vote_question.setText(CONSTANTS.QUESTION_DOWNLOADED.getJSONObject(pagePos).getJSONObject("questionlist").getString("question"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void sendVote() {
        pDialog.setMessage("Mengirim Data...");
        pDialog.setCancelable(false);
        pDialog.show();

        //*Json Request*//*
        String url = CONFIG.STOREVOTE_ADDRESS;
        JSONObject vote_data_send = new JSONObject();

        try {
            //sLog.d("KIRIM",jsonObject_vote.toString());
            vote_data_send.put("data", jsonObject_vote.toString());
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                    url,
                    vote_data_send,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                //pDialog.hide();
                                Log.d("STORE VOTE", response.getString("status"));
                                pDialog.dismiss();

                                Intent itn = new Intent(Vote.this, VoteResult.class);
                                itn.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(itn);

                            } catch (JSONException e) {
                                pDialog.dismiss();
                                Toast.makeText(Vote.this, "Tidak dapat mengirim", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pDialog.dismiss();
                            //Log.e("ERROR", error.getMessage());
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                                Toast.makeText(Vote.this, "Jaringan Bermasalah",
                                        Toast.LENGTH_LONG).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(Vote.this, "Username atau Password salah",
                                        Toast.LENGTH_LONG).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(Vote.this, "Server Error",
                                        Toast.LENGTH_LONG).show();
                            } else if (error instanceof NetworkError) {
                                Toast.makeText(Vote.this, "Network Error",
                                        Toast.LENGTH_LONG).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(Vote.this, "Parse Error",
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    //headers.put("Content-Type", "application/json");
                    headers.put("Accept", "application/json");
                    headers.put("Authorization", "Bearer " + CONSTANTS.ACCESS_TOKEN);
                    return headers;
                }
            };

            //add request to queue
            requestQueue.add(jsonObjectRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getQuestionData() {
        pDialog.setMessage("Mengambil Data Pertanyaan...");
        pDialog.setCancelable(false);
        pDialog.show();

        //*Json Request*//*
        String url = CONFIG.QUESTIONDATA_ADDRESS;

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        CONSTANTS.QUESTION_DOWNLOADED = response;
                        showQuestion();
                        pDialog.dismiss();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(getBaseContext(), "Jaringan Bermasalah",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(getBaseContext(), "Username atau Password salah",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(getBaseContext(), "Server Error",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(getBaseContext(), "Network Error",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(getBaseContext(), "Parse Error",
                                    Toast.LENGTH_LONG).show();
                        }
                    }

                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + CONSTANTS.ACCESS_TOKEN);
                return headers;
            }
        };

        //add request to queue
        requestQueue.add(jsonArrayRequest);
    }
}