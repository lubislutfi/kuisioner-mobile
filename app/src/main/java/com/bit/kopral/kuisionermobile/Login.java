package com.bit.kopral.kuisionermobile;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import CONST.CONFIG;
import CONST.CONSTANTS;

public class Login extends AppCompatActivity {
    Button btn_login_submit;

    EditText et_login_username;
    EditText et_login_password;

    //kebutuhan http request
    RequestQueue requestQueue;
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.login);
        getSupportActionBar().hide();
        initialize();
    }

    private void initialize() {

        requestQueue = Volley.newRequestQueue(this); // 'this' is the Context

        pDialog = new ProgressDialog(this);

        et_login_username = (EditText) findViewById(R.id.et_login_username);
        et_login_password = (EditText) findViewById(R.id.et_login_password);
    }

    public void kuisionerclick(View v) {
        switch (v.getId()) {
            case R.id.btn_login_submit:
                login();
                break;
        }
    }

    public void login() {
        pDialog.setMessage("Melakukan Login...");
        pDialog.show();

        //*Json Request*//*
        String url = CONFIG.LOGIN_ADDRESS;

        Map<String, String> loginParams = new HashMap<String, String>();

        loginParams.put("grant_type", "password");
        loginParams.put("client_id", CONFIG.CLIENT_ID);
        loginParams.put("client_secret", CONFIG.CLIENT_SECRET);
        loginParams.put("username", et_login_username.getText().toString());
        loginParams.put("password", et_login_password.getText().toString());
        loginParams.put("scope", "");

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                url,
                new JSONObject(loginParams),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //pDialog.hide();
                            CONSTANTS.ACCESS_TOKEN = response.getString("access_token");
                            CONSTANTS.REFRESH_TOKEN = response.getString("refresh_token");

                            getUserData();

                        } catch (JSONException e) {
                            pDialog.dismiss();
                            Toast.makeText(Login.this, "Tidak dapat melakukan login", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(Login.this, "Jaringan Bermasalah",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(Login.this, "Username atau Password salah",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(Login.this, "Server Error",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(Login.this, "Network Error",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(Login.this, "Parse Error",
                                    Toast.LENGTH_LONG).show();
                        }
                    }

                });

        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }

    public void getUserData() {
        pDialog.setMessage("Mengambil Data User...");

        //Log.d("TOKEN MAIN", CONSTANTS.ACCESS_TOKEN);

        //*Json Request*//*
        String url = CONFIG.USERDATA_ADDRESS;

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.d("response",response.toString());

                         CONSTANTS.ID = response.getJSONObject(0).getString("id");
                            CONSTANTS.USERNAME = response.getJSONObject(0).getString("username");
                            CONSTANTS.EMAIL = response.getJSONObject(0).getString("email");
                            CONSTANTS.NAME = response.getJSONObject(0).getString("name");
                            CONSTANTS.BRANCH_ID = response.getJSONObject(0).getString("branch_id");
                            CONSTANTS.BRANCH_NAME = response.getJSONObject(0).getJSONObject("branches").getString("name");
                           CONSTANTS.USERGROUP_ID = response.getJSONObject(0).getString("usergroup_id");
                            CONSTANTS.USERGROUP_NAME = response.getJSONObject(0).getJSONObject("usergroup").getString("name");

                            pDialog.dismiss();

                            Intent itn = new Intent(Login.this, Main.class);
                            startActivity(itn);

                           // pDialog.hide();
                        } catch (JSONException e) {
                            pDialog.dismiss();
                            Toast.makeText(getBaseContext(), "Tidak dapat mengambil data", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(getBaseContext(), "Jaringan Bermasalah",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(getBaseContext(), "Username atau Password salah",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(getBaseContext(), "Server Error",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(getBaseContext(), "Network Error",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(getBaseContext(), "Parse Error",
                                    Toast.LENGTH_LONG).show();
                        }
                    }

                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + CONSTANTS.ACCESS_TOKEN);
                return headers;
            }
        };

        //add request to queue
        requestQueue.add(jsonArrayRequest);
    }
}
