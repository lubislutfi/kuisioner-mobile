package com.bit.kopral.kuisionermobile;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import CONST.CONFIG;
import CONST.CONSTANTS;

public class Main extends AppCompatActivity {

    FloatingActionButton fab_main_vote;

    //kebutuhan http request
    RequestQueue requestQueue;
    ProgressDialog pDialog;

    TextView tv_main_username;
    TextView tv_main_cabang;
    TextView tv_main_usergroup;
    TextView tv_main_waktu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.main);
        //getSupportActionBar().hide();

        //SharedPreferences mySharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        //String server_address = mySharedPreferences.getString("server_address", "192.168.1.1");
        initialize();
    }

    private void initialize() {

        requestQueue = Volley.newRequestQueue(this); // 'this' is the Context
        pDialog = new ProgressDialog(this);

        tv_main_username = (TextView) findViewById(R.id.tv_main_username);
        tv_main_cabang = (TextView) findViewById(R.id.tv_main_cabang);
        tv_main_usergroup = (TextView) findViewById(R.id.tv_main_usergroup);
        tv_main_waktu = (TextView) findViewById(R.id.tv_main_waktu);

        tv_main_username.setText(CONSTANTS.NAME);
        tv_main_cabang.setText(CONSTANTS.BRANCH_NAME);
        tv_main_usergroup.setText(CONSTANTS.USERGROUP_NAME);
        tv_main_waktu.setText(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));

        fab_main_vote = (FloatingActionButton) findViewById(R.id.fab_main_vote);
        fab_main_vote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itn = new Intent(Main.this, Vote.class);
                startActivity(itn);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent itn;
        switch (item.getItemId()) {
            case R.id.action_settings:
                itn = new Intent(Main.this, Settings.class);
                startActivity(itn);
                break;
            case R.id.action_logout:
                itn = new Intent(Main.this, Settings.class);
                startActivity(itn);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

}
