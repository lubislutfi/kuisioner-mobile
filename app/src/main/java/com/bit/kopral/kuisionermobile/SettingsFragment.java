package com.bit.kopral.kuisionermobile;

import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * Created by kopral on 11/15/16.
 */

public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }

}
