package util;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.util.Log;

/**
 * Created by kopral on 1/18/17.
 */

public class AudioPlayerHelper {
    MediaPlayer mediaPlayer = null;
    Context cont;

    public AudioPlayerHelper(Context val_cont) {
        this.cont = val_cont;
    }

    public void playSoundFX(String fileName) {
        mediaPlayer = new MediaPlayer();

        try {
            AssetFileDescriptor afd = cont.getAssets().openFd(fileName);
            mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            afd.close();
            mediaPlayer.prepare();
        } catch (final Exception e) {
            Log.d("Media Error",e.toString());
            e.printStackTrace();
        }

        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.stop();
                mediaPlayer.reset();
                mediaPlayer.release();
            }
        });
    }

}
