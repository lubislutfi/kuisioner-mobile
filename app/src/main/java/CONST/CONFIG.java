package CONST;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by kopral on 1/16/17.
 */

public class CONFIG {

    //context
    public static Context CONT;

    //connection
    private static String IP = "192.168.1.67";
    private static String ADDRESS = "http://"+IP+"/kuisioner-ntt-web/public";

    //authentication config
    public static String LOGIN_ADDRESS = ADDRESS + "/oauth/token";
    public static String CLIENT_ID = "1";
    public static String CLIENT_SECRET = "QsMLPNQ8WaxSeDvcDj7fTMUIrr3l7Ynh3UjUdSOn";

    //data config
    public static String USERDATA_ADDRESS = ADDRESS + "/api/user";
    public static String QUESTIONDATA_ADDRESS = ADDRESS + "/api/getquestion";
    public static String STOREVOTE_ADDRESS = ADDRESS + "/api/storevote";
}
